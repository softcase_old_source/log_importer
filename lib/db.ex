defmodule LogImporter.Database do
  require Logger

  @import "SELECT fn_import($1, $2, $3, $4, $5, $6, $7, $8)"
  @uptimes "SELECT fn_import_uptimes($1, $2, $3, $4)"

  def import(%{date: date, node: node, rx: rx, tx: tx, rcnt: rcnt, tcnt: tcnt, reconnects: reconnects, vips: vips, uptimes: uptimes}, server_node) do
    Ecto.Adapters.SQL.query!(LogImporter.Database.Repo, @import, [date, node, "0", rx, tx, rcnt, tcnt, reconnects])
    Enum.each(vips, fn (%{itf: itf, rx: vrx, tx: vtx, rcnt: vrcnt, tcnt: vtcnt}) ->
      Ecto.Adapters.SQL.query!(LogImporter.Database.Repo, @import, [date, node, itf, vrx, vtx, vrcnt, vtcnt, 0])
    end)
    # Logger.info inspect(uptimes)
    Enum.each(uptimes, fn (%{time: time, operation: operation}) ->
      dt = date <>" "<> time
      Ecto.Adapters.SQL.query!(LogImporter.Database.Repo, @uptimes, [server_node, dt, node, operation])
    end)

  end

end
