defmodule LogImporter do
  use Application
  require Logger

  @timeout 600_000

  def version do
    {:ok, version} = :application.get_key(:log_importer, :vsn)
    version
  end

  def start(_type, []) do
    Logger.error "usage: ./log_importer --config conf.xml --server NODE"
  end

  def start(_type, [config: config_file, server: server_node]) do
  # def start(_type, _opts) do
  #   config_file = System.get_env("CONFIG")
  #   server_node = System.get_env("SERVER_NODE")

    Logger.info "START"
    import Supervisor.Spec, warn: false

    Configuration.start_link(config_file)
    dbuser = Configuration.get("DATABASE/DBUSER")
    dbpass = Configuration.get("DATABASE/DBPASS")
    dbhost = Configuration.get("DATABASE/DBHOST")
    dbport = Configuration.get("DATABASE/DBPORT")
    dbname = Configuration.get("DATABASE/DBNAME")

    date = Date.utc_today |> Date.add(-1) |> Date.to_string |> String.replace("-", "")

    workdir = Configuration.get("APPLICATION/WORKDIR")
    mask = Configuration.get("APPLICATION/FILEMASK")

    workdir = if String.length(workdir) > 2 and String.last(workdir) != "/",
              do: workdir <> "/", else: workdir

    mask = if String.length(mask) == 0, do: "*#{date}.log", else: mask

    children = [
      supervisor(LogImporter.Database.Repo, [[
        url: "ecto://#{dbuser}:#{dbpass}@#{dbhost}:#{dbport}/#{dbname}"
      ]]),
      :poolboy.child_spec(:worker, [
        name: {:local, :worker},
        worker_module: LogImporter.Worker,
        size: 5,
        max_overflow: 2,
        timeout: @timeout
      ], [])
    ]

    opts = [strategy: :one_for_one, name: :log_importer]
    {:ok, pid} = Supervisor.start_link(children, opts)

    "#{workdir}#{mask}"
    |> Path.wildcard()
    |> Enum.map(fn filename ->
      Task.async(fn ->
        :poolboy.transaction(:worker, &GenServer.call(&1, {:parse, [filename, server_node]}, @timeout), @timeout)
      end)
    end)
    |> Enum.each(fn task -> IO.puts(Task.await(task, @timeout)) end)

    Logger.info "STOP"
    {:ok, pid}
  end

end
