defmodule LogImporter.Database.Repo do
  use Ecto.Repo,
  otp_app: :log_importer,
  adapter: Ecto.Adapters.Postgres
end
