defmodule Start do

  defp parse_args(args) do
    {options, _, _} = OptionParser.parse(args, switches: [foo: :string])
    options
  end

  def main(args \\ []) do
    case LogImporter.start(:normal, parse_args(args)) do
      # {:ok, _} -> Process.sleep(:infinity)
      _ -> nil
    end
  end

end
