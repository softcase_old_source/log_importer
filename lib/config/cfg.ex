defmodule Configuration do
  def start_link(config_file) do
    Agent.start_link(fn -> load(config_file) end, [name: __MODULE__])
  end

  defp load(config_file) do
    %{file: config_file, config:
      File.read!(config_file) |> XmlParser.parse
    }
  end

  def save, do: File.write!(Map.get(get(), :file), Map.get(get(), :config) |> XmlBuilder.generate)
  def set(path, value) do
   Agent.update(__MODULE__,
     fn (state)-> %{state | config: set(Map.get(state, :config), path, value)} end)
  end

  def get(path), do: Agent.get(__MODULE__, fn (state)-> get(Map.get(state, :config), path) end)
  def get, do: Agent.get(__MODULE__, fn (state)-> state end)

  def get(config, path) do
    String.split(path, "/")
    |> Enum.reduce(config, fn (x, acc)-> find_value(acc, x) end)
    |> get_value
  end

  defp get_value({:param, %{name: name, value: value}, ""}), do: value
  defp get_value({:param, %{name: name}, child}) do
    for {:value, %{value: value}, ""} <- child, into: [], do: value
  end

  defp get_value(_), do: [""]

  defp set(config, path, value) do
    set_value(config, String.split(path, "/"), value)
  end

  defp set_value({:param, %{name: name, value: val}, v}, path, value) do
    cond do
      name==hd(path) ->
        {:param, %{name: name, value: value}, v}
      true ->
        {:param, %{name: name, value: val}, v}
    end
  end

  defp set_value(input, path, value) when is_list(input) do
    list=Enum.reduce(input, [], fn (x, acc)->[set_value(x, path, value) | acc] end) |> Enum.reverse
    if find_value(list, hd(path))=={nil, nil, nil} do
      cond do
        Enum.count(path)==1 ->
          list=[{:param, %{name: hd(path), value: value}, nil} | list]
        true ->
          list=[{:table, %{name: hd(path)}, set_value([], tl(path), value)} | list]
      end
    end
    list
  end

  defp set_value({:table, %{name: name}, v}, path, value) when is_list(v) do
    cond do
      name==hd(path) ->
        {:table, %{name: name}, set_value(v, tl(path), value)}
      true ->
        {:table, %{name: name}, v}
    end
  end

  defp set_value({type, p, v}, path, value) do
    {type, p, set_value(v, path, value)}
  end

  defp find_value(nil, value) do
    {nil, nil, nil}
  end

  defp find_value(cfg, value) when is_tuple(cfg) do
    elem(cfg, 2) |> find_value(value)
  end

  defp find_value_if(cfg, condit, val) do
    Enum.find(cfg, {nil, nil, nil},
      fn (c)->
        case elem(c, 1) do
          %{name: name, value: value} -> name==condit and value==val
          %{tag: name} ->
            case find_value_if(elem(c, 2), condit, val) do
              {nil, nil, nil} -> false
              _ -> true
            end
          _ -> false
        end
      end)
  end

  defp find_value(cfg, value) when is_list(cfg) do
    cond do
      String.contains?(value, "=") ->
        [a, b]=String.split(value, "=")
        find_value_if(cfg, a, b)
      true ->
        Enum.find(cfg, {nil, nil, nil},
          fn (c)->
            case elem(c, 1) do
              %{name: name, value: _} -> name==value
              %{name: name} -> name==value
              _ -> false
            end
          end)
    end
  end
end
