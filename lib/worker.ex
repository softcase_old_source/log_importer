defmodule LogImporter.Worker do
  use GenServer

  def start_link(_) do
    GenServer.start_link(__MODULE__, nil)
  end

  def init(_) do
    {:ok, nil}
  end

  def handle_call({:parse, [filename, server_node]}, _from, state) do
    {:reply, LogImporter.Freader.parse(filename, server_node), state}
  end
end
