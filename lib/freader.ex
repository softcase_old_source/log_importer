defmodule LogImporter.Freader do
  require Logger

  defmodule State do
    defstruct date: 0, node: 0, rx: 0, tx: 0, itfs: []
  end

  def process(filemask, server_node) do
    Logger.info "List #{filemask}"

    Path.wildcard(filemask)
    |> Enum.reduce([], fn(x, acc)-> [Task.async(LogImporter.Freader, :parse, [x, server_node]) | acc] end)
    |> Enum.map(&Task.await(&1, 360000))

    Logger.info "processed all files"
  end

  def parse(fname, server_node) do
    node=String.split(fname, ".") |> hd |> String.split("_") |> Enum.at(1)
    date=String.split(fname, ".") |> hd |> String.split("_") |> Enum.at(2)

    if String.match?(node, ~r/^[0-9]+$/) do
      Logger.info "Parse #{fname}"
      File.stream!(fname)
      |> Stream.map(&String.split(&1, " "))
      |> Enum.reduce(%{date: date, node: node, rx: 0, tx: 0, rcnt: 0, tcnt: 0, reconnects: 0, vips: [], uptimes: []}, fn (x, acc) ->
          %{rx: rx, tx: tx, rcnt: rcnt, tcnt: tcnt, reconnects: reconnects, vips: vips, uptimes: uptimes}=acc
          case Enum.at(x, 1) do
            "VIT" ->
              case Enum.at(x, 3) do
                "OnRead"  ->
                  add=String.to_integer(Enum.at(x, 5))
                  Map.merge(acc, %{rx: (rx+add), rcnt: rcnt+1})
                "OnWrite" ->
                  add=String.to_integer(Enum.at(x, 5))
                  Map.merge(acc, %{tx: (tx+add), tcnt: tcnt+1})
                "Authentication" ->
                  Map.merge(acc, %{reconnects: reconnects+1})
                  |> Map.merge(%{uptimes: [%{time: Enum.at(x, 0), operation: "1"} | uptimes]})
                "closing" ->
                  Map.merge(acc, %{uptimes: [%{time: Enum.at(x, 0), operation: "0"} | uptimes]})

                _ -> acc
              end
              "VIP" -> %{acc | vips: parse_vip(vips, x)}
              "SNMP:" -> %{acc | vips: parse_snmp(vips, x)}
              _     -> acc
          end
        end)
      # |> IO.inspect
      |> LogImporter.Database.import(server_node)

      Logger.info "Imported #{fname}"
    end
  end

  defp parse_vip(vips, str) do
    vip=Enum.filter(vips, fn (%{itf: itf})-> itf==Enum.at(str, 2) end)
    vips=vips -- vip

    case Enum.at(str, 4) do
      "OnRead"  ->
        case vip do
          [] -> [%{itf: Enum.at(str, 2), tx: String.to_integer(Enum.at(str, 6)), rx: 0, tcnt: 1, rcnt: 0} | vips]
          [%{itf: itf, rx: rx, tx: tx, tcnt: tcnt, rcnt: rcnt}] -> [%{itf: itf, tx: (tx+String.to_integer(Enum.at(str, 6))), rx: rx, tcnt: tcnt+1, rcnt: rcnt} | vips]
        end
      "OnWrite" ->
        case vip do
          [] -> [%{itf: Enum.at(str, 2), tx: 0, rx: String.to_integer(Enum.at(str, 6)), tcnt: 0, rcnt: 1} | vips]
          [%{itf: itf, rx: rx, tx: tx, tcnt: tcnt, rcnt: rcnt}] -> [%{itf: itf, tx: tx, rx: (rx+String.to_integer(Enum.at(str, 6))), tcnt: tcnt, rcnt: rcnt+1} | vips]
        end
      _ -> vips ++ vip
    end
  end

  defp parse_snmp(vips, str) do
    vip=Enum.filter(vips, fn (%{itf: itf})-> itf=="1" end)
    vips=vips -- vip

    case Enum.at(str, 2) do
      "OnRead"  ->
        case vip do
          [] -> [%{itf: "1", tx: String.to_integer(Enum.at(str, 3)), rx: 0, tcnt: 1, rcnt: 0} | vips]
          [%{itf: itf, rx: rx, tx: tx, tcnt: tcnt, rcnt: rcnt}] -> [%{itf: itf, tx: (tx+String.to_integer(Enum.at(str, 3))), rx: rx, tcnt: tcnt+1, rcnt: rcnt} | vips]
        end
      "OnWrite" ->
        case vip do
          [] -> [%{itf: "1", tx: 0, rx: String.to_integer(Enum.at(str, 3)), tcnt: 0, rcnt: 1} | vips]
          [%{itf: itf, rx: rx, tx: tx, tcnt: tcnt, rcnt: rcnt}] -> [%{itf: itf, tx: tx, rx: (rx+String.to_integer(Enum.at(str, 3))), tcnt: tcnt, rcnt: rcnt+1} | vips]
        end
      _ -> vips ++ vip
    end
  end

end
