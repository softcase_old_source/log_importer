FROM elixir:1.9-alpine as releaser

ARG MIX_ENV
ENV HOME=/app

RUN apk add --no-cache build-base cmake git

RUN mix do local.hex --force, local.rebar --force

WORKDIR $HOME

COPY ./mix.exs $HOME/
RUN MIX_ENV=${MIX_ENV} mix do deps.get, deps.compile

COPY ./lib $HOME/lib
RUN MIX_ENV=${MIX_ENV} mix do compile, escript.build
RUN ls -lha /app

########################################################################
FROM alpine:3.9

ARG MIX_ENV
ENV LANG=en_US.UTF-8 \
    HOME=/app \
    REPLACE_OS_VARS=true \
    SHELL=/bin/sh \
    APP=log_importer

RUN apk add --no-cache bash ncurses-libs openssl tzdata
RUN echo "Europe/Moscow" > /etc/timezone
RUN date

# ENV CONFIG=$HOME/config.xml
# VOLUME $HOME/config.xml

COPY --from=releaser $HOME/$APP $HOME
RUN ls -lha $HOME

ENTRYPOINT ["/app/log_importer"]
