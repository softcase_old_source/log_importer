defmodule LogImporter.Mixfile do
  use Mix.Project

  # def project do
  #   [
  #     app: :log_importer,
  #     version: "0.1.0",
  #     elixir: "~> 1.4",
  #     start_permanent: Mix.env() == :prod,
  #     deps: deps(),
  #     escript: [main_module: Start],
  #     releases: [
  #       log_importer: [
  #         include_executables_for: [:unix],
  #         applications: [runtime_tools: :permanent],
  #         path: "./rel/log_importer",
  #         quiet: true
  #       ]
  #     ]
  #   ]
  # end


  def project do
    [app: :log_importer,
     version: "0.1.0",
     elixir: "~> 1.4",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     escript: [main_module: Start],
     deps: deps()]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    # Specify extra applications you'll use from Erlang/Elixir
    [
      applications:
      [
        :logger,
        :postgrex,
        :ecto,
        :xml_parser,
        :xml_builder,
        :flow,
        :poolboy
      ]
      # extra_applications: [:logger, :runtime_tools]
      # mod: {LogImporter, []}
    ]
  end

  # Dependencies can be Hex packages:
  #
  #   {:my_dep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:my_dep, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      {:postgrex, ">= 0.9.1"},
      {:ecto, "~> 2.2"},
      {:xml_parser, "~> 0.1.0"},
      {:xml_builder, "~> 0.0.6"},
      {:flow, "~> 0.11"},
      {:poolboy, "~> 1.5.1"}
    ]
  end
end
