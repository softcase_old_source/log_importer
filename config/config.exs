use Mix.Config

config :log_importer, LogImporter.Database.Repo,
  adapter: Ecto.Adapters.Postgres,
  pool_size: 5,
  timeout: 600_000,
  pool_timeout: 600_000,
  connect_timeout: 600_000

config :logger, level: :info
# config :logger, level: :debug
